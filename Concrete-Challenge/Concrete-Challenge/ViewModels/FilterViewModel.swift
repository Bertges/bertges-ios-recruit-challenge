//
//  FilterViewModel.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 03/07/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import Foundation

class FilterViewModel {
    
    // MARK: - Properties
    
    var dates: [Int] = []
    var genres: [Int] = []
    var movies = MovieMO.fetch().map({ MovieViewModel($0) })
    
    var datesValue: String {
        let filteredDates = movies.filter({ dates.contains($0.year) }).map({ $0.releaseYear })
        return filteredDates.joined(separator: ", ")
    }
    
    var genresValue: String {
        let filteredGenres = GenresManager.allGenres().filter({ genres.contains(Int($0.id)) })
        
        return filteredGenres.map({ $0.name ?? "" }).filter({ !$0.isEmpty }).joined(separator: ", ")
    }
    
    var allFilteringDates = MovieMO.fetch().filter({ MovieViewModel($0).year != -1 }).map({ MovieViewModel($0).year })
    var dateOptions: [String] {
        return allFilteringDates.map({ String($0) })
    }
    
    var allFilteringGenres = GenresManager.allGenres().compactMap({ Int($0.id) })
    var genreOptions: [String] {
        return GenresManager.allGenres().compactMap({ $0.name })
    }
    
    // MARK: - Init
    
    init(dates: [Int], genres: [Int]) {
        self.dates = dates
        self.genres = genres
    }
}
