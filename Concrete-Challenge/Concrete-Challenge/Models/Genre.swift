//
//  Genre.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 03/07/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import Foundation

class Genre: Codable {
    var id: Int16
    var name: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
    }
}
