//
//  RequestManager.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 30/06/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import Alamofire
import UIKit

enum ResponseResult {
    case success(Any),
    failure(Error?)
}

struct RequestSettings: Codable {
    var production: String = ""
    var homologation: String = ""
    var development: String = ""
    var apiKey: String = ""
    var configuration: String = ""
    var popularMovies: String = ""
    var genreList: String = ""
    
    init() {
    }
}

class RequestManager {
    
    static let shared = RequestManager()
    var imagesBaseURL = "http://image.tmdb.org/t/p/"
    var settings: RequestSettings {
        let decoder = PropertyListDecoder()
        guard let url = Bundle.main.url(forResource: "RequestSettings", withExtension: "plist"),
            let data = try? Data(contentsOf: url),
            let environmentSettings = try? decoder.decode(RequestSettings.self, from: data) else { return RequestSettings() }
        
        return environmentSettings
    }
    var baseURL: String! {
        #if RELEASE
        return settings.production
        #else
        return settings.development
        #endif
    }
    
    init() {
    }
    
    //MARK: - Base Request
    
    func request(_ endpoint: String, needsAPIKey: Bool, parameters: String = "", completion: @escaping (_ respose: ResponseResult) -> Void) {
        
        guard let url = URL(string: baseURL+endpoint+"?"+settings.apiKey+parameters) else {
            completion(.failure(nil))
            return
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        Alamofire.request(url).responseJSON { (response) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            switch response.result {
            case .success:
                if let result = response.result.value {
                    completion(.success(result))
                } else {
                    completion(.failure(nil))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func updateConfiguration(completion: @escaping (() -> Void)) {
        request(settings.configuration, needsAPIKey: true) { response in
            switch response {
            case .success(let result):
                guard let responseDict = result as? [String: Any] else {
                    DispatchQueue.main.async {
                        completion()
                    }
                    return
                }
                if let imageConfiguration = responseDict["images"] as? [String: Any], let imagesBaseURL = imageConfiguration["base_url"] as? String {
                    RequestManager.shared.imagesBaseURL = imagesBaseURL
                }
                completion()
            case .failure(let error):
                print(error ?? "updateConfiguration error")
                completion()
            }
        }
    }
}
