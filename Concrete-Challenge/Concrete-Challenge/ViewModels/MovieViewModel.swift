//
//  MovieViewModel.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 30/06/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import UIKit

class MovieViewModel {
    
    // MARK: - Properties
    
    private var movie: Movie!
    
    var title: String {
        return movie.title
    }
    
    var posterURL: URL? {
        return URL(string: "\(RequestManager.shared.imagesBaseURL)original\(movie.posterPath ?? "")")
    }
    
    var releaseDate: Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-mm-dd"
        
        guard let release = movie.release else {
            return nil
        }
        
        return formatter.date(from: release)
    }
    
    var releaseYear: String {
        if year == -1 {
            return "Release year: ---"
        }
        
        return String(year)
    }
    
    var year: Int {
        guard let date = releaseDate else {
            return -1
        }
        
        return Calendar.current.component(Calendar.Component.year, from: date)
    }
    
    var overview: String {
        return movie.overview
    }
    
    var isFavorite: Bool {
        return MovieMO.fetch(movie.id) != nil
    }
    
    var favoriteIconColor: UIColor {
        return isFavorite ? UIColor.primaryYellow : UIColor.lightGray
    }
    
    var genresIds: [Int] {
        return movie.genres
    }
    
    // MARK: - Init
    
    init(_ movie: Movie) {
        self.movie = movie
    }
    
    init(_ movie: MovieMO) {
        self.movie = Movie(movie)
    }
    
    func toggleFavorite() {
        if isFavorite {
            unfavorite()
        } else {
            favorite()
        }
    }
    
    private func favorite() {
        movie.favorite()
    }
    
    func unfavorite() {
        guard let movieMO = MovieMO.fetch(movie.id) else { return }
        movieMO.unfavorite()
    }
}
