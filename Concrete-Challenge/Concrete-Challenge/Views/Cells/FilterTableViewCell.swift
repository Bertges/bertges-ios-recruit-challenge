//
//  FilterTableViewCell.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 04/07/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    // MARK: - Properties
    
    static var nib = UINib(nibName: "FilterTableViewCell", bundle: nil)
    static var identifier = "FilterTableViewCell"
    
    static func instance(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, description: String, value: String) -> FilterTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? FilterTableViewCell else {
            return FilterTableViewCell()
        }
        
        cell.descriptionLabel.text = description
        cell.valueLabel.text = value
        
        return cell
    }
}
