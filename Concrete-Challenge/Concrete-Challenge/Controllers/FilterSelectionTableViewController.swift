//
//  FilterSelectionTableViewController.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 04/07/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import UIKit

protocol FilterSelectionDelegate {
    func didSelectRows(_ rows: [Int])
}

class FilterSelectionTableViewController: UITableViewController {
    
    // MARK: - Properties
    
    var options: [String] = []
    var filterSelectionDelegate: FilterSelectionDelegate?
    var selectedIndexes: [Int] = []
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsMultipleSelection = true
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    // MARK: - Datasource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = options[indexPath.row]
        cell.accessoryView = selectedIndexes.contains(indexPath.row) ? UIImageView(image: #imageLiteral(resourceName: "check-icon")) : nil
        cell.selectionStyle = .none
        
        return cell
    }
    
    // MARK: - Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        if selectedIndexes.contains(row), let index = selectedIndexes.index(of: row) {
            selectedIndexes.remove(at: index)
        } else {
            selectedIndexes.append(row)
        }
        filterSelectionDelegate?.didSelectRows(selectedIndexes)
        tableView.reloadData()
    }

}
