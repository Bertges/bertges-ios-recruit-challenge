//
//  FavoriteTableViewCell.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 02/07/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import UIKit

class FavoriteTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: - Properties
    
    static let identifier = "FavoriteTableViewCell"
    static let nib = UINib(nibName: "FavoriteTableViewCell", bundle: nil)
    static let height: CGFloat = 150
    
    // MARK: - Instance
    
    static func instance(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, movie: MovieViewModel) -> FavoriteTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? FavoriteTableViewCell else {
            return FavoriteTableViewCell()
        }
        
        cell.yearLabel.text = movie.releaseYear
        cell.titleLabel.text = movie.title
        cell.descriptionLabel.text = movie.overview
        if let url = movie.posterURL {
            cell.posterImageView.setImage(url: url, placeholderImage: #imageLiteral(resourceName: "movie-placeholder"))
        }
        
        return cell
    }
}
