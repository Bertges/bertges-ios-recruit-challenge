//
//  MovieListViewModel.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 01/07/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import UIKit

class MovieListViewModel {
    // MARK: - Properties
    
    private var movies: [MovieViewModel] = []
    var filteredMovies: [MovieViewModel] {
        var results = movies.filter({
            $0.title.hasPrefix(searchText)
        })
        
        if !filteringYears.isEmpty {
            results = results.filter({ filteringYears.contains($0.year) })
        }
        
        if !filteringGenres.isEmpty {
            results = results.filter({ filteringGenres.contains(where: $0.genresIds.contains) })
        }
        
        return results
    }
    let numberOfColumns: CGFloat = 2
    let ratio: CGFloat = 1.60
    var searchText: String = ""
    var filteringYears: [Int] = []
    var filteringGenres: [Int] = []
    var hasFilter: Bool {
        return !filteringGenres.isEmpty || !filteringYears.isEmpty
    }
    var page = 1
    
    // MARK: - Init
    
    init() {
    }
    
    // MARK: - Methods
    
    func requestMovies(page: Int, completion: @escaping ((_ success: Bool) -> Void)) {
        RequestManager.shared.request(RequestManager.shared.settings.popularMovies, needsAPIKey: true, parameters: "&page=\(page)") { (response) in
            switch response {
            case .success(let result):
                
                guard let responseDict = result as? [String: Any], let resultDict = responseDict["results"]else {
                    DispatchQueue.main.async {
                        completion(false)
                    }
                    return
                }
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: resultDict, options: .prettyPrinted)
                    let decoder = JSONDecoder()
                    let movieList = try decoder.decode([Movie].self, from: jsonData)
                    self.movies.append(contentsOf: movieList.map({ MovieViewModel($0) }))
                    
                    DispatchQueue.main.async {
                        completion(true)
                    }
                } catch (let error) {
                    print("error - \(error)")
                    DispatchQueue.main.async {
                        completion(false)
                    }
                }
                
            case .failure(let error):
                print(error ?? "requestMovies error")
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
    }
    
    func fetchFavorites() {
        movies = MovieMO.fetch().map({ MovieViewModel($0) })
    }
    
    func cellSize(_ collectionView: UICollectionView) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width / self.numberOfColumns) - 10, height: (collectionView.frame.size.width / 2) * self.ratio)
    }
    
    func remove(at index: Int) {
        let movieToRemove = movies[index]
        
        movieToRemove.unfavorite()
        movies.remove(at: index)
    }
    
    func removeFilters() {
        filteringGenres = []
        filteringYears = []
    }
}
