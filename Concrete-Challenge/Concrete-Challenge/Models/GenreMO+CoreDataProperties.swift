//
//  GenreMO+CoreDataProperties.swift
//  
//
//  Created by Felippe Bertges on 03/07/2018.
//
//

import Foundation
import CoreData


extension GenreMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<GenreMO> {
        return NSFetchRequest<GenreMO>(entityName: "GenreMO")
    }

    @NSManaged public var id: Int16
    @NSManaged public var name: String?
    @NSManaged public var movieGenre: MovieMO?

}
