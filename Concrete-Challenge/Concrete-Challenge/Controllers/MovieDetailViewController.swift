//
//  MovieDetailViewController.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 01/07/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    
    // MARK: - Properties
    
    let nib = "MovieDetailViewController"
    let viewModel: MovieViewModel!
    
    // MARK: - Init
    
    init(_ viewModel: MovieViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nib, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
    }
    
    // MARK: - User Interface
    
    private func setupUI() {
        title = "Movie"
        
        scrollView.isHidden = true
        loadingView.startAnimating()
        updateUI()
        requestGenres()
    }
    
    private func updateUI() {
        if let url = viewModel.posterURL {
        posterImageView.setImage(url: url, placeholderImage: #imageLiteral(resourceName: "movie-placeholder"))
        }
    
        titleLabel.text = viewModel.title
        yearLabel.text = viewModel.releaseYear
        descriptionLabel.text = viewModel.overview
        favoriteButton.tintColor = viewModel.favoriteIconColor
    }
    
    private func requestGenres() {
        let genresNames = self.localGenres()
        
        if genresNames.count == viewModel.genresIds.count {
            genresNamesLoaded(genresNames)
        } else {
            GenresManager.requestGenres(completion: {
                self.genresNamesLoaded(self.localGenres())
            })
        }
    }
    
    private func localGenres() -> [String] {
        return GenresManager.genresNames(viewModel.genresIds).filter({ !$0.isEmpty })
    }
    
    private func genresNamesLoaded(_ names: [String]) {
        scrollView.isHidden = false
        loadingView.stopAnimating()
        genresLabel.text = names.joined(separator: ", ")
    }
    
    // MARK: - Action

    @IBAction func favorite(_ sender: UIButton) {
        viewModel.toggleFavorite()
        sender.tintColor = viewModel.favoriteIconColor
    }
}
