//
//  MovieMO+CoreDataProperties.swift
//  
//
//  Created by Felippe Bertges on 02/07/2018.
//
//

import Foundation
import CoreData


extension MovieMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MovieMO> {
        return NSFetchRequest<MovieMO>(entityName: "MovieMO")
    }

    @NSManaged public var id: Int64
    @NSManaged public var title: String?
    @NSManaged public var overview: String?
    @NSManaged public var releaseDate: String?
    @NSManaged public var posterPath: String?
    @NSManaged public var genre: Set<GenreMO>?

}

// MARK: Generated accessors for genre
extension MovieMO {

    @objc(addGenreObject:)
    @NSManaged public func addToGenre(_ value: GenreMO)

    @objc(removeGenreObject:)
    @NSManaged public func removeFromGenre(_ value: GenreMO)

    @objc(addGenre:)
    @NSManaged public func addToGenre(_ values: NSSet)

    @objc(removeGenre:)
    @NSManaged public func removeFromGenre(_ values: NSSet)

}
