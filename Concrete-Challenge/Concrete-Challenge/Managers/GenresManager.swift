//
//  GenresManager.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 03/07/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import CoreData
import Foundation

class GenresManager {
    
    
    
    static func requestGenres(completion: (() -> Void)?) {
        RequestManager.shared.request(RequestManager.shared.settings.genreList, needsAPIKey: true) { response in
            switch response {
            case .success(let result):
                
                guard let responseDict = result as? [String: Any], let resultDict = responseDict["genres"] else {
                    DispatchQueue.main.async {
                        completion?()
                    }
                    return
                }
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: resultDict, options: .prettyPrinted)
                    let decoder = JSONDecoder()
                    let genreList = try decoder.decode([Genre].self, from: jsonData)
                    save(genreList)
                    
                    DispatchQueue.main.async {
                        completion?()
                    }
                } catch (let error) {
                    print("error - \(error)")
                    DispatchQueue.main.async {
                        completion?()
                    }
                }
                
            case .failure(let error):
                print(error ?? "request genres error")
                DispatchQueue.main.async {
                    completion?()
                }
            }
        }
    }
    
    static func save(_ list: [Genre]) {
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "GenreMO")
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        let _ = try? CoreDataManager.context.execute(request)
        
        for genre in list {
            if let genreMO = GenreMO.fetch([Int64(genre.id)]).first {
                genreMO.name = genre.name
            } else {
                let genreMO = GenreMO(context: CoreDataManager.context)
                genreMO.id = genre.id
                genreMO.name = genre.name
            }
        }
        
        CoreDataManager.saveContext()
    }
    
    static func genresNames(_ ids: [Int]) -> [String] {
        let ids64 = ids.map({ Int64($0) })
        return GenreMO.fetch(ids64).map({ $0.name ?? "" })
    }
    
    static func genres(_ ids: [Int]) -> [GenreMO] {
        let ids64 = ids.map({ Int64($0) })
        return GenreMO.fetch(ids64)
    }
    
    static func allGenres() -> [GenreMO] {
        return GenreMO.fetch()
    }
}
