//
//  Concrete_ChallengeTests.swift
//  Concrete-ChallengeTests
//
//  Created by Felippe Bertges on 30/06/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import XCTest
@testable import Concrete_Challenge

class Concrete_ChallengeTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testServicePerformance() {
        let movieListViewModel = MovieListViewModel()
        
        let expectation = XCTestExpectation(description: "expects that the request returns up to 5 seconds")
        
        movieListViewModel.requestMovies(page: 1) { (success) in
            if success {
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: 5)
    }
    
    func testFavorite() {
        let movie = Movie()
        movie.title = "Test Movie"
        movie.id = -3
        movie.favorite()
        
        XCTAssert(MovieMO.fetch(-3) != nil)
    }
    
    func testUnfavorite() {
        let movie = Movie()
        movie.title = "Test Movie"
        movie.id = -4
        movie.favorite()
        
        movie.unfavorite()
        XCTAssert(MovieMO.fetch(-4) == nil)
    }
    
    func testSearchFilter() {
        let movieListViewModel = MovieListViewModel()
        let movie = Movie()
        movie.title = "Test Movie"
        movie.id = 0
        movie.favorite()
        
        let secondMovie = Movie()
        secondMovie.title = "Second Test Movie"
        secondMovie.id = 1
        secondMovie.favorite()
        
        movieListViewModel.fetchFavorites()
        movieListViewModel.searchText = "Test"
        
        XCTAssert(movieListViewModel.filteredMovies.count == 1)
    }
    
}
