//
//  Extensions.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 30/06/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import AlamofireImage
import UIKit

extension UIViewController {
    func setupNavigationBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
}

extension UIImageView {
    func setImage(url: URL, placeholderImage: UIImage) {
        self.af_setImage(withURL: url, placeholderImage: placeholderImage, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: true, completion: nil)
    }
}

extension UIColor {
    static let primaryYellow:UIColor = UIColor(named: "primaryYellow")!
    static let secondaryYellow:UIColor = UIColor(named: "secondaryYellow")!
    static let primaryBlue:UIColor = UIColor(named: "primaryBlue")!
}
