//
//  FilterTableViewController.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 03/07/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import UIKit

enum FilterType: String {
    case date = "Date",
    genre = "Genre"
    
    var index: Int {
        switch self {
        case .genre:
            return 1
        default:
            return 0
        }
    }
}

protocol FilterDelegate {
    func apply(years: [Int], genres: [Int])
}

class FilterViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    
    var options: [FilterType] = [.date, .genre]
    var viewModel: FilterViewModel!
    var type: FilterType?
    var delegate: FilterDelegate?
    
    // MARK: - Instance
    
    static func instance(_ viewModel: FilterViewModel, delegate: FilterDelegate) -> UINavigationController {
        let storyboard = UIStoryboard(name: "Filter", bundle: nil)
        let navigation = storyboard.instantiateInitialViewController() as! UINavigationController
        let controller = navigation.viewControllers.first as! FilterViewController
        controller.viewModel = viewModel
        controller.delegate = delegate
        return navigation
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    // MARK: - User Interface
    
    private func setupUI() {
        title = "Filter"
        registerCells()
    }
    
    private func registerCells() {
        tableView.register(FilterTableViewCell.nib, forCellReuseIdentifier: FilterTableViewCell.identifier)
    }
    
    // MARK: - Actions
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func apply(_ sender: UIButton) {
        delegate?.apply(years: viewModel.dates, genres: viewModel.genres)
        dismiss(animated: true, completion: nil)
    }
    
    private func showDates() {
        type = .date
        let controller = FilterSelectionTableViewController()
        controller.title = "Date"
        controller.options = viewModel.dateOptions
        controller.selectedIndexes = viewModel.dates.compactMap({ viewModel.allFilteringDates.index(of: $0) })
        controller.filterSelectionDelegate = self
        navigationController?.pushViewController(controller, animated: true)
    }
    
    private func showGenres() {
        type = .genre
        let controller = FilterSelectionTableViewController()
        controller.title = "Genre"
        controller.options = viewModel.genreOptions
        controller.selectedIndexes = viewModel.genres.compactMap({ viewModel.allFilteringGenres.index(of: $0) })
        controller.filterSelectionDelegate = self
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension FilterViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let option = options[indexPath.row]
        
        switch option {
        case .date:
            return FilterTableViewCell.instance(tableView, cellForRowAt: indexPath, description: option.rawValue, value: viewModel.datesValue)
        default:
            return FilterTableViewCell.instance(tableView, cellForRowAt: indexPath, description: option.rawValue, value: viewModel.genresValue)
        }
        
    }
}

extension FilterViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let option = options[indexPath.row]
        
        switch option {
        case .date:
            showDates()
        default:
            showGenres()
        }
    }
}

extension FilterViewController: FilterSelectionDelegate {
    func didSelectRows(_ rows: [Int]) {
        guard let filterType = type else  { return }
        
        switch filterType {
        case .date:
            viewModel.dates = rows.map({ viewModel.allFilteringDates[$0] })
        case .genre:
            viewModel.genres = rows.map({ viewModel.allFilteringGenres[$0] })
        }
    }
    
    
}
