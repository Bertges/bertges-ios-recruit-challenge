//
//  ErrorView.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 03/07/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import UIKit

enum ErrorType {
    case notFound(searchText: String),
    genericError
}

protocol ErrorViewDelegate {
    func errorViewAction()
}

class ErrorView: UIView {
    
    // MARK: - Outlets

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    // MARK: - Properties
    
    private var visible: Bool = false
    var type: ErrorType! {
        didSet {
            switch type! {
            case .genericError:
                descriptionLabel.text = "An error occurred. Please, try again."
                imageView.image = #imageLiteral(resourceName: "error-icon")
                actionButton.isHidden = false
                
            case .notFound(let searchText):
                descriptionLabel.text = "Your search for \"\(searchText)\" has no results."
                imageView.image = #imageLiteral(resourceName: "search-icon")
                actionButton.isHidden = true
            }
        }
    }

    var delegate: ErrorViewDelegate?
    
    // MARK: - Instance
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commomInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commomInit()
    }
    
    private func commomInit() {
        Bundle.main.loadNibNamed("ErrorView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    // MARK: - Actions
    
    func show(_ viewController: UIViewController, type: ErrorType,  delegate: ErrorViewDelegate) {
        if !visible {
            self.delegate = delegate
            viewController.view.addSubview(self)
            self.type = type
            visible = true
        }
    }
    
    func hide() {
        visible = false
        self.removeFromSuperview()
    }
    
    @IBAction func action(_ sender: UIButton) {
        delegate?.errorViewAction()
    }
}
