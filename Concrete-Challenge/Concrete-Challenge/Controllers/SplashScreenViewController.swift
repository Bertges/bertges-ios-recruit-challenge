//
//  SplashScreenViewController.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 30/06/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.proceed()
        }
    }
    
    private func proceed() {
        self.performSegue(withIdentifier: "mainSegue", sender: self)
    }
}
