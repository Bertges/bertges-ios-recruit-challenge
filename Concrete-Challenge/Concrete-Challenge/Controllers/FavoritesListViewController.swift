//
//  FavoritesListViewController.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 02/07/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import UIKit

class FavoritesListViewController: MovieListViewController {
    
    // MARK: - Outlets

    @IBOutlet weak var removeFilterButton: UIButton!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchFavorites()
    }
    
    // MARK: - User Interface
    
    private func setupUI() {
        removeFilterButton.isHidden = true
        registerCells()
        setupNavigationBar()
        fetchFavorites()
        addFilter()
    }
    
    private func addFilter() {
        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "filter-icon"), style: .done, target: self, action: #selector(filter))
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    private func registerCells() {
        tableView?.register(FavoriteTableViewCell.nib, forCellReuseIdentifier: FavoriteTableViewCell.identifier)
    }
    
    // MARK: - Datasource
    
    private func fetchFavorites() {
        viewModel.fetchFavorites()
        tableView?.reloadData()
    }
    
    // MARK: - Actions
    
    @IBAction func removeFilter(_ sender: UIButton) {
        viewModel.removeFilters()
        removeFilterButton.isHidden = true
        tableView?.reloadData()
    }
    
    // MARK: - ErrorView
    
    override func errorViewFrame() -> CGRect {
        return tableView?.superview?.frame ?? CGRect.zero
    }
}

// MARK: - UITableViewDataSource

extension FavoritesListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.filteredMovies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let movie = viewModel.filteredMovies[indexPath.row]
        return FavoriteTableViewCell.instance(tableView, cellForRowAt: indexPath, movie: movie)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Unfavorite") { (rowAction: UITableViewRowAction, indexPath: IndexPath) -> Void in
            self.viewModel.remove(at: indexPath.row)
            self.tableView?.deleteRows(at: [indexPath], with: .automatic)
        }
        
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("Deleted")
            
            self.viewModel.remove(at: indexPath.row)
            self.tableView?.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    // MARK: - Filter
    
    @objc private func filter() {
        let viewModel = FilterViewModel(dates:self.viewModel.filteringYears, genres:self.viewModel.filteringGenres)
        present(FilterViewController.instance(viewModel, delegate: self), animated: true, completion: nil)
    }
}

// MARK: - UITableViewDelegate

extension FavoritesListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return FavoriteTableViewCell.height
    }
}

extension FavoritesListViewController: FilterDelegate {
    func apply(years: [Int], genres: [Int]) {
        viewModel.filteringGenres = genres
        viewModel.filteringYears = years
        removeFilterButton.isHidden = !viewModel.hasFilter
        tableView?.reloadData()
    }
}
