//
//  DefaultsManager.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 04/07/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import Foundation

class DefaultsManager {
    
    // MARK: - Properties
    
    static let userDefaults = UserDefaults.standard
    
    // MARK: - Getters / Setters
    
    private static func value(for key: String) -> Any? {
        return userDefaults.value(forKey: key)
    }
    
    private static func set(_ key: String, value: Any) {
        userDefaults.setValue(value, forKey: key)
    }
    
    static func didLaunchAppBefore(_ flag: Bool) {
        set("didLaunchAppBefore", value: flag)
    }
    
    static func didLaunchAppBefore() -> Bool {
        return value(for: "didLaunchAppBefore") as? Bool ?? false
    }
}
