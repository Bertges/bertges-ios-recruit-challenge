//
//  MovieMO+CoreDataClass.swift
//  
//
//  Created by Felippe Bertges on 01/07/2018.
//
//

import Foundation
import CoreData

@objc(MovieMO)
public class MovieMO: NSManagedObject {
    
    func save() -> Bool {
        do {
            try CoreDataManager.context.save()
            return true
        } catch {
            print("error saving \(self.title ?? "")")
            return false
        }
    }
    
    func setup(_ movie: Movie) {
        self.title = movie.title
        self.id = movie.id
        self.overview = movie.overview
        self.posterPath = movie.posterPath
        self.releaseDate = movie.release
        for genre in GenresManager.genres(movie.genres) {
            addToGenre(genre)
        }
    }
    
    static func fetch(_ id: Int64) -> MovieMO? {
        return fetch(NSPredicate(format: "id == %d", id)).first
    }
    
    static func fetch(_ predicate: NSPredicate? = nil) -> [MovieMO] {
        let fetchRequest: NSFetchRequest<MovieMO> = MovieMO.fetchRequest()
        
        if let filter = predicate {
            fetchRequest.predicate = filter
        }
        
        do {
            return try CoreDataManager.context.fetch(fetchRequest)
        } catch {
            print("error fetching movies")
            return []
        }
    }
    
    func favorite() {
        let _ = save()
    }
    
    func unfavorite() {
        CoreDataManager.context.delete(self)
        let _ = save()
    }
}
