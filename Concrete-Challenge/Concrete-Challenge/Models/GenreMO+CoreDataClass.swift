//
//  GenreMO+CoreDataClass.swift
//  
//
//  Created by Felippe Bertges on 03/07/2018.
//
//

import Foundation
import CoreData

@objc(GenreMO)
public class GenreMO: NSManagedObject {
    static func fetch(_ ids: [Int64]) -> [GenreMO] {
        return fetch(NSPredicate(format: "self.id IN %@", ids))
    }
    
    static func fetch(_ predicate: NSPredicate? = nil) -> [GenreMO] {
        let fetchRequest: NSFetchRequest<GenreMO> = GenreMO.fetchRequest()
        
        if let filter = predicate {
            fetchRequest.predicate = filter
        }
        
        do {
            return try CoreDataManager.context.fetch(fetchRequest)
        } catch {
            print("error fetching movies")
            return []
        }
    }
}
