//
//  MovieListCollectionViewCell.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 30/06/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import UIKit

class MovieListCollectionViewCell: UICollectionViewCell {

    // MARK: - Outlets

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var favoriteImageView: UIImageView!

    // MARK: - Properties
    
    static let identifier = "MovieListCollectionViewCell"
    static let nib = UINib(nibName: "MovieListCollectionViewCell", bundle: nil)
    
    // MARK: - Instance
    
    static func instance(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, movie: MovieViewModel) -> MovieListCollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? MovieListCollectionViewCell else {
            return MovieListCollectionViewCell()
        }
        
        cell.titleLabel.text = movie.title
        cell.favoriteImageView.tintColor = movie.favoriteIconColor
        if let url = movie.posterURL {
            cell.coverImageView.setImage(url: url, placeholderImage: #imageLiteral(resourceName: "movie-placeholder"))
        }
        
        return cell
    }
}
