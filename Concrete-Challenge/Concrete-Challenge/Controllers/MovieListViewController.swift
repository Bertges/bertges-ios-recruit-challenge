//
//  MovieListViewController.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 02/07/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import UIKit

class MovieListViewController: UIViewController, ErrorViewDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var tableView: UITableView?
    
    // MARK: - Properties
    
    var viewModel = MovieListViewModel()
    private var _errorView: ErrorView?
    var errorView: ErrorView {
        guard let view = _errorView else {
            _errorView = ErrorView(frame: errorViewFrame())
            return _errorView!
        }
        
        return view
    }
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationBar()
    }
    
    // MARK: - ErrorViewDelegate
    
    func errorViewAction() {
    }
    
    func errorViewFrame() -> CGRect {
        if let collection = collectionView, let frame = collection.superview?.frame {
            return frame
        } else if let frame = tableView?.frame {
            return frame
        }
        
        return CGRect.zero
    }
}

// MARK: - UISearchBarDelegate

extension MovieListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.searchText = searchText
        
        if viewModel.filteredMovies.isEmpty {
            showSearchError()
        } else {
            errorView.hide()
        }
        
        collectionView?.reloadData()
        tableView?.reloadData()
        
        collectionView?.setContentOffset(CGPoint.zero, animated: true)
        tableView?.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func showSearchError() {
        errorView.show(self, type: .notFound(searchText: viewModel.searchText), delegate: self)
    }
}
