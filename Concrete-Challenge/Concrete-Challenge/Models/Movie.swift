//
//  Movie.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 30/06/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import Foundation


class Movie: Codable {
    var id: Int64!
    var title: String = ""
    var genres: [Int] = []
    var overview: String = ""
    var release: String?
    var posterPath: String?
    
    enum CodingKeys: String, CodingKey {
        case genres = "genre_ids"
        case release = "release_date"
        case posterPath = "poster_path"
        
        case id
        case title
        case overview
    }
    
    func favorite() {
        let movieMO = MovieMO(context: CoreDataManager.context)
        movieMO.setup(self)
        movieMO.favorite()
    }
    
    func unfavorite() {
        guard let movieMO = MovieMO.fetch(self.id) else { return }
        let _ = movieMO.unfavorite()
    }
    
    init() {
    }
    
    init(_ movieMO: MovieMO) {
        self.title = movieMO.title ?? ""
        self.id = movieMO.id
        self.overview = movieMO.overview ?? ""
        self.posterPath = movieMO.posterPath ?? ""
        self.release = movieMO.releaseDate
        if let genreList = movieMO.genre {
            self.genres = genreList.compactMap({ Int($0.id) })
        }
    }
}

class PopularMovieResponse: Codable {
    var page: Int = 0
    var totalResults: Int = 0
    var totalPages: Int = 0
    var movies: [Movie] = []
    
    enum CodingKeys: String, CodingKey {
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case movies = "result"
        
        case page
    }
}
