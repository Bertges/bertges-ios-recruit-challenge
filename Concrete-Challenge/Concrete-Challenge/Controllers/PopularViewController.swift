//
//  MovieListViewController.swift
//  Concrete-Challenge
//
//  Created by Felippe Bertges on 30/06/2018.
//  Copyright © 2018 Felippe Bertges. All rights reserved.
//

import UIKit

private enum ViewStatus {
    case loading,
    failed,
    succeed,
    stopped,
    loadingNextPage
    
    init(_ flag: Bool) {
        self = flag ? .succeed : .failed
    }
}

class PopularViewController: MovieListViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var loadingNextPageView: UIActivityIndicatorView!
    
    // MARK: - Properties
    
    private var status: ViewStatus = .stopped {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateConfiguration()
        collectionView?.reloadData()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.reloadData()
    }
    
    // MARK: - User Interface
    
    private func setupUI() {
        setupNavigationBar()
        registerCells()
    }
    
    private func registerCells() {
        collectionView?.register(MovieListCollectionViewCell.nib, forCellWithReuseIdentifier: MovieListCollectionViewCell.identifier)
    }
    
    private func updateUI() {
        switch status {
        case .failed:
            updateToFailedView()
        case .succeed:
            updateToSucceedView()
        case .loading:
            updateToLoadingView()
        case .stopped:
            updateToStoppedView()
        case .loadingNextPage:
            updateToLoadingNextPageView()
        }
    }
    
    private func updateToFailedView() {
        loadingView.stopAnimating()
        collectionView?.isHidden = true
        errorView.show(self, type: .genericError, delegate: self)
    }
    
    private func updateToSucceedView() {
        viewModel.page += 1
        loadingView.stopAnimating()
        loadingNextPageView.stopAnimating()
        loadingNextPageView.superview?.isHidden = true
        collectionView?.isHidden = false
        collectionView?.reloadData()
//        collectionView?.setContentOffset(CGPoint(x: 0, y: collectionView!.contentSize.height - viewModel.cellSize(collectionView!).height), animated: false)
    }
    
    private func updateToStoppedView() {
        loadingView.stopAnimating()
        loadingNextPageView.stopAnimating()
        loadingNextPageView.superview?.isHidden = true
        collectionView?.isHidden = true
    }
    
    private func updateToLoadingView() {
        loadingView.startAnimating()
        loadingNextPageView.stopAnimating()
        loadingNextPageView.superview?.isHidden = true
        collectionView?.isHidden = true
        errorView.hide()
    }
    
    private func updateToLoadingNextPageView() {
        loadingNextPageView.startAnimating()
        loadingNextPageView.superview?.isHidden = false
        loadingView.stopAnimating()
        collectionView?.isHidden = false
    }
    
    // MARK: - Request
    
    private func updateConfiguration() {
        status = .loading
        RequestManager.shared.updateConfiguration { [weak self] in
            self?.requestMovies()
        }
    }
    
    private func requestMovies() {
        status = .loading
        viewModel.requestMovies(page: viewModel.page) { [weak self] success in
            self?.status = ViewStatus(success)
        }
    }
    
    private func requestNextMovies() {
        status = .loadingNextPage
        viewModel.requestMovies(page: viewModel.page) { [weak self] success in
            self?.status = ViewStatus(success)
        }
    }
    
    override func errorViewAction() {
        requestMovies()
    }
}

// MARK: - UICollectionViewDataSource

extension PopularViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.filteredMovies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return MovieListCollectionViewCell.instance(collectionView, cellForItemAt: indexPath, movie: viewModel.filteredMovies[indexPath.item])
    }
}

// MARK: - UICollectionViewDelegate

extension PopularViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(MovieDetailViewController.init(viewModel.filteredMovies[indexPath.item]), animated: true)
    }
}

//MARK: - UICollectionViewDelegateFlowLayout

extension PopularViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return viewModel.cellSize(collectionView)
    }
}

// MARK: -

extension PopularViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if bottomEdge >= scrollView.contentSize.height && status != .loadingNextPage && status != .loading {
            requestNextMovies()
        }
    }
}
